<!DOCTYPE html>
<html lang="en" class="no-js">
    <head>
        <meta name="viewport"  http-equiv="Content-Type" content="width=100%; initial-scale=1; maximum-scale=1; minimum-scale=1; user-scalable=no;text/html; charset=UTF-8" />
        <title>Takum</title>
        <link href="static/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
        <!-- Fin -->
        <link rel="shortcut icon" href="/favicon.ico" type="image/x-icon">
        <link rel="icon" href="/favicon.ico" type="image/x-icon">
        <!-- base -->
        <link href="static/base/css/base.css" rel="stylesheet">
        <!-- fin -->
    </head>
    <body ng-app="prueba">
        <div ng-view></div>
        <footer class="container-fluid text-center">
            <p>Andr&eacute;s Felipe Rojas Jaramillo</p>
            <p>Contacto : 313 622 9998</p>
        </footer>
        <!-- fin -->

    </body>

    <!-- base -->
    <script src="static/vendor/jquery/jquery-1.11.1.min.js"></script>
    <script src="static/vendor/bootstrap/js/bootstrap.min.js"></script>

    <script src="static/vendor/angular/angular.js"></script>
    <script src="static/vendor/angular/angular-route.js"></script>
    <script src="static/vendor/angular/angular-cookies.js"></script>

    <script src="static/base/js/base.js"></script>

    <script src="modules/authentication/services.js"></script>
    <script src="modules/authentication/controllers.js"></script>
    <script src="modules/home/controllers.js"></script>

    <script src="static/vendor/angular/angular.min.js"></script>		
</body>
</html>
