-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 08-03-2018 a las 23:41:09
-- Versión del servidor: 10.1.9-MariaDB
-- Versión de PHP: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `portal`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `productos`
--

CREATE TABLE `productos` (
  `PRO_ID` int(10) NOT NULL,
  `PRO_NOMBRE` varchar(50) NOT NULL,
  `PRO_DESCRIPCION` varchar(100) NOT NULL,
  `PRO_COSTO` int(10) NOT NULL,
  `USU_ID` int(10) NOT NULL,
  `PRO_CATEGORIA` varchar(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `productos`
--

INSERT INTO `productos` (`PRO_ID`, `PRO_NOMBRE`, `PRO_DESCRIPCION`, `PRO_COSTO`, `USU_ID`, `PRO_CATEGORIA`) VALUES
(1, 'Shampoo', 'Shampoo para cabello rizado', 15000, 123456, 'Aseo Personal'),
(2, 'Crema dental', 'Lavado bucal', 4600, 123456, 'Aseo Personal'),
(3, 'Jabon liquido', 'Jabon para manos', 8500, 654321, 'Aseo Personal'),
(4, 'Licuadora', 'Electrodomestico', 94000, 123456, 'Electrodomesticos'),
(5, 'Batidora', 'Electrodomestico', 145000, 654321, 'Electrodomesticos'),
(6, 'Cafetera', 'Electrodomestico', 58000, 123456, 'Electrodomesticos');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `roles`
--

CREATE TABLE `roles` (
  `ROL_ID` int(11) NOT NULL,
  `ROL_NOMBRE` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `roles`
--

INSERT INTO `roles` (`ROL_ID`, `ROL_NOMBRE`) VALUES
(1, 'Vendedor'),
(2, 'Asesor');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `USU_ID` int(10) NOT NULL,
  `USU_NOMBRE` varchar(50) NOT NULL,
  `USU_APELLIDO` varchar(50) NOT NULL,
  `ROL_ID` int(11) NOT NULL,
  `USU_CLAVE` varchar(15) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`USU_ID`, `USU_NOMBRE`, `USU_APELLIDO`, `ROL_ID`, `USU_CLAVE`) VALUES
(123456, 'Pepe', 'Perez', 1, 'ab123'),
(654321, 'Oscar', 'Vallejo', 2, '@@789');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `productos`
--
ALTER TABLE `productos`
  ADD PRIMARY KEY (`PRO_ID`);

--
-- Indices de la tabla `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`ROL_ID`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`USU_ID`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
